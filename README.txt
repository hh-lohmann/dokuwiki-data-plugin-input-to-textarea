dataPluginInput2Textarea Plugin for DokuWiki

Works as an additon to the data plugin (https://www.dokuwiki.org/plugin:data),
without the data plugin it will do nothing!

The data plugin's editor uses only INPUT elements, for fields with more
text a TEXTAREA would be better. dataPluginInput2Textarea adds JavaScript code
that transforms defined data plugin INPUTs to TEXTAREAs, definition takes place
in sript.js inside of the jQuery block by a data plugin field name as parameter
for a transformDataPluginInputFieldToTextarea call.

The JavaScript way was chosen due to more flexibility over trying to change the
data plugin itself.

All documentation for this plugin can be found at
https://bitbucket.org/hh-lohmann/dokuwiki-data-plugin-input-to-textarea

If you install this plugin manually, make sure it is installed in
lib/plugins/dataPluginInput2Textarea/ - if the folder is called different it
will not work!

Please refer to http://www.dokuwiki.org/plugins for additional info
on how to install plugins in DokuWiki.

----
Copyright (C) hh.lohmann <hh.lohmann@yahoo.de>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2 of the License

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

See the COPYING file in your DokuWiki folder for details
