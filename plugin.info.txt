base   dataPluginInput2Textarea
author hh.lohmann
email  hh.lohmann@yahoo.de
date   2014-05-13
name   data plugin INPUT to TEXTAREA
desc   The data plugin's editor uses only INPUT elements, for fields with more text a TEXTAREA would be better.
url    https://bitbucket.org/hh-lohmann/dokuwiki-data-plugin-input-to-textarea