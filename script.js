/*
* "Activation" and definition of data fields that are to be transformed to textareas
*/
jQuery (
  function () {
    transformDataPluginInputFieldToTextarea ( 'Kurzbeschreibung' );
  }
);

/*
* Main function: do transformation
*/
function transformDataPluginInputFieldToTextarea ( strDataPluginField ) {
  var arrDataPluginInputLines = getDataPluginInputLine() ;
  for ( var i = 0 ; i < arrDataPluginInputLines.length ; i++ ) {
    if ( arrDataPluginInputLines[i].innerHTML == strDataPluginField + ':' ) {
      mkInputToTextarea ( arrDataPluginInputLines[i].nextSibling.firstChild.lastChild ) ;
      break; 
    }
  }
}

/*
* Helper function: traversing data plugin's editor interface structure
*/
function getDataPluginInputLine () {
  return jQuery ( 'fieldset.plugin__data' ).children( 'table' ).children( 'tbody' ).children( 'tr' ).children( 'td');
}

/*
* Helper function: replacing an INPUT by a TEXTAREA, adding some CSS, replacing line breaks by space (the data plugin expects a (one line) INPUT here, so only the first (or the only) line of a TEXTAREA will "survive")
*/
function mkInputToTextarea ( objGoal ) {
  var objReplace = document.createElement ( 'textarea' ) ;
  objReplace.name = objGoal.name ;
  objReplace.innerHTML = objGoal.value ;
  objReplace.style.width = '100%' ;
  objReplace.style.height = '10em' ;
  objReplace.onblur = function () { this.value = this.value.replace ( /[\r\n]/g , ' ') ; } ;
  var objGoalParent = objGoal.parentNode ;
  objGoalParent.replaceChild ( objReplace, objGoal ) ;
}